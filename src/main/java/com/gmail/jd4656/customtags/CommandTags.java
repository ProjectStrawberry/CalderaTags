package com.gmail.jd4656.customtags;

import com.gmail.jd4656.InventoryManager.InventoryClickHandler;
import com.gmail.jd4656.InventoryManager.InventoryManager;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.StringUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CommandTags implements TabExecutor {
    private CustomTags plugin;

    CommandTags(CustomTags p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Chat chat = plugin.getChat();

        if (args.length > 0) {
            if (args[0].equals("help")) {
                sender.sendMessage(ChatColor.GOLD + "/tags commands:");
                sender.sendMessage(ChatColor.GOLD + "/tags - Displays a list of tags you can use.");
                if (sender.hasPermission("customtags.manage")) {
                    sender.sendMessage(ChatColor.GOLD + "/tags set <player> \"[Custom Tag] \" - Gives a player a custom tag.");
                    sender.sendMessage(ChatColor.GOLD + "/tags remove <player> - Displays a list of custom tags a player has and lets you remove a tag.");
                    sender.sendMessage(ChatColor.GOLD + "/tags force <player> - Displays a list of tags a player has access to and lets you forcibly change their tag.");
                }
                return true;
            }
            if (args[0].equals("clear") && sender instanceof Player) {
                chat.setPlayerPrefix(null, (Player) sender, null);
                sender.sendMessage(ChatColor.GOLD + "Your prefix has been reset.");
                return true;
            }
            if (args[0].equals("reload")) {
                if (!sender.hasPermission("customtags.reload")) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }

                plugin.reloadConfig();
                plugin.config = plugin.getConfig();

                sender.sendMessage(ChatColor.DARK_GREEN + "Configuration reloaded.");
                return true;
            }

            if (args[0].equals("force")) {
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                    return true;
                }
                if (!sender.hasPermission("customtags.manage")) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                if (args.length < 2) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /tags force <player>");
                    return true;
                }

                Player targetPlayer = Bukkit.getPlayer(args[1]);
                Player player = (Player) sender;

                if (targetPlayer == null) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That player is not online.");
                    return true;
                }

                String[] groupArr = chat.getPlayerGroups(targetPlayer);
                LinkedList<String> prefixes = new LinkedList<>();

                List<String> bannedTags = plugin.config.getStringList("bannedTags");

                for (String aGroupArr : groupArr) {
                    String curPrefix = chat.getGroupPrefix(targetPlayer.getWorld(), aGroupArr);
                    if (curPrefix == null || curPrefix.length() < 1 || bannedTags.contains(curPrefix)) continue;
                    prefixes.add(curPrefix);
                }

                try {
                    String sql;
                    PreparedStatement pstmt;
                    ResultSet rs;
                    Connection c = plugin.getConnection();

                    sql = "SELECT * FROM tags WHERE user=?";
                    pstmt = c.prepareStatement(sql);
                    pstmt.setString(1, targetPlayer.getUniqueId().toString());
                    rs = pstmt.executeQuery();

                    while (rs.next()) {
                        prefixes.add(rs.getString("tag"));
                    }

                    rs.close();
                    pstmt.close();

                    if (prefixes.size() < 1) {
                        player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That player does not have access to any tags.");
                        return true;
                    }

                    InventoryManager manager = new InventoryManager(ChatColor.translateAlternateColorCodes('&', chat.getPlayerPrefix(targetPlayer) + " " + targetPlayer.getDisplayName()), 54, plugin);

                    for (int i = 0; i < prefixes.size(); i++) {
                        ItemStack item = new ItemStack(Material.NAME_TAG, 1);
                        ItemMeta meta = item.getItemMeta();
                        meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', prefixes.get(i)));
                        item.setItemMeta(meta);

                        manager.withItem(i, item);
                    }

                    manager.withEventHandler(new InventoryClickHandler() {
                        @Override
                        public void handle(InventoryClickEvent event) {
                            Player player1 = (Player) event.getWhoClicked();
                            ItemStack clicked = event.getCurrentItem();
                            if (clicked.getType().equals(Material.AIR)) {
                                event.setCancelled(true);
                                return;
                            }

                            ItemMeta meta = clicked.getItemMeta();
                            event.setCancelled(true);
                            String name = meta.getDisplayName();

                            chat.setPlayerPrefix(null, targetPlayer, name);
                            player1.closeInventory();
                            player1.sendMessage(ChatColor.GOLD + "That players prefix has been changed to " + ChatColor.translateAlternateColorCodes('&', chat.getPlayerPrefix(targetPlayer)));
                        }
                    });

                    manager.show(player);
                    return true;
                } catch (Exception e) {
                    plugin.getLogger().severe("/tags force: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                    return true;
                }
            }

            if (args[0].equals("remove") || args[0].equals("delete") || args[0].equals("rem") || args[0].equals("del")) {
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                    return true;
                }
                if (!sender.hasPermission("customtags.manage")) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                if (args.length < 2) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /tags remove <player>");
                    return true;
                }

                if (!plugin.uuidCache.containsKey(args[1].toLowerCase())) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That user does not exist.");
                    return true;
                }

                Player targetPlayer = Bukkit.getPlayer(args[1]);
                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(plugin.uuidCache.get(args[1].toLowerCase()));
                Player player = (Player) sender;

                try {
                    LinkedList<String> prefixes = new LinkedList<>();
                    String sql;
                    PreparedStatement pstmt;
                    ResultSet rs;
                    Connection c = plugin.getConnection();

                    pstmt = c.prepareStatement("SELECT count(*) FROM tags WHERE user=?");
                    pstmt.setString(1, offlinePlayer.getUniqueId().toString());
                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That player has no custom tags.");
                        pstmt.close();
                        return true;
                    }

                    sql = "SELECT * FROM tags WHERE user=?";
                    pstmt = c.prepareStatement(sql);
                    pstmt.setString(1, offlinePlayer.getUniqueId().toString());
                    rs = pstmt.executeQuery();

                    while (rs.next()) {
                        prefixes.add(rs.getString("tag"));
                    }

                    rs.close();

                    if (prefixes.size() < 1) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That user has no custom tags.");
                        return true;
                    }

                    InventoryManager manager = new InventoryManager(ChatColor.GOLD + (targetPlayer == null ? offlinePlayer.getName() : targetPlayer.getDisplayName()) + "'s Custom Tags", 54, plugin);

                    for (int i = 0; i < prefixes.size(); i++) {
                        ItemStack item = new ItemStack(Material.NAME_TAG, 1);
                        ItemMeta meta = item.getItemMeta();
                        meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', prefixes.get(i)));
                        item.setItemMeta(meta);

                        manager.withItem(i, item);
                    }

                    manager.withEventHandler(new InventoryClickHandler() {
                        @Override
                        public void handle(InventoryClickEvent event) {
                            Player player1 = (Player) event.getWhoClicked();
                            ItemStack clicked = event.getCurrentItem();
                            if (clicked.getType().equals(Material.AIR)) {
                                event.setCancelled(true);
                                return;
                            }

                            ItemMeta meta = clicked.getItemMeta();
                            event.setCancelled(true);
                            String name = meta.getDisplayName();

                            InventoryManager confirmDelete = new InventoryManager(ChatColor.GOLD + "Confirm deletion of " + name, 54, plugin);

                            ItemStack yes = new ItemStack(Material.LIME_CONCRETE, 1);
                            ItemMeta yesMeta = yes.getItemMeta();
                            yesMeta.setDisplayName("Confirm");
                            yes.setItemMeta(yesMeta);

                            ItemStack no = new ItemStack(Material.RED_CONCRETE, 1);
                            ItemMeta noMeta = yes.getItemMeta();
                            noMeta.setDisplayName("Cancel");
                            no.setItemMeta(noMeta);

                            confirmDelete.withItem(0, yes);
                            confirmDelete.withItem(1, no);

                            confirmDelete.withEventHandler(new InventoryClickHandler() {
                                @Override
                                public void handle(InventoryClickEvent event1) {
                                    Player player2 = (Player) event1.getWhoClicked();
                                    ItemStack clicked1 = event1.getCurrentItem();
                                    if (clicked1.getType().equals(Material.AIR)) {
                                        event1.setCancelled(true);
                                        return;
                                    }

                                    if (clicked1.getType().equals(Material.LIME_CONCRETE)) {
                                        try {
                                            PreparedStatement pstmt1;

                                            pstmt1 = plugin.getConnection().prepareStatement("DELETE FROM tags WHERE user=? AND tag=?");
                                            pstmt1.setString(1, offlinePlayer.getUniqueId().toString());
                                            pstmt1.setString(2, name);
                                            pstmt1.executeUpdate();
                                            pstmt1.close();

                                            plugin.cacheUsers();

                                            player2.closeInventory();
                                            chat.setPlayerPrefix(null, offlinePlayer, null);
                                            player2.sendMessage(ChatColor.GOLD + "You have removed the tag \"" + name + ChatColor.GOLD + "\" from " + (targetPlayer == null ? offlinePlayer.getName() : targetPlayer.getDisplayName()));
                                        } catch (Exception e) {
                                            plugin.getLogger().severe("/tags remove 2: " + e.getClass().getName() + ": " + e.getMessage());
                                            e.printStackTrace();
                                            sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                                        }
                                    }

                                    if (clicked1.getType().equals(Material.RED_CONCRETE)) {
                                        player2.closeInventory();
                                    }
                                }
                            });

                            confirmDelete.show(player1);
                        }
                    });

                    manager.show(player);
                    return true;
                } catch (Exception e) {
                    plugin.getLogger().severe("/tags remove: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                    return true;
                }

            }
            if (args[0].equals("set") || args[0].equals("add")) {
                if (!sender.hasPermission("customtags.manage")) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to manage tags.");
                    return true;
                }
                if (args.length < 3) {
                    sender.sendMessage(ChatColor.GOLD + "Usage: /tags set <player> \"[Custom Tag] \"");
                    return true;
                }

                if (!plugin.uuidCache.containsKey(args[1].toLowerCase())) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
                    return true;
                }

                Player targetPlayer = Bukkit.getPlayer(args[1]);
                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(plugin.uuidCache.get(args[1].toLowerCase()));

                StringBuilder tag = new StringBuilder();
                boolean first = true;

                for (int i = 2; i < args.length; i++) {
                    if (!first) tag.append(" ");
                    tag.append(args[i]);
                    if (first) first = false;
                }

                if (tag.toString().contains("\"")) {
                    tag = new StringBuilder(tag.toString().replaceAll("\"", ""));
                }

                String finalTag = ChatColor.translateAlternateColorCodes('&', tag.toString());

                try {
                    String sql;
                    PreparedStatement pstmt;
                    ResultSet rs;
                    Connection c = plugin.getConnection();

                    sql = "SELECT * FROM tags WHERE user=?";
                    pstmt = c.prepareStatement(sql);
                    pstmt.setString(1, offlinePlayer.getUniqueId().toString());
                    rs = pstmt.executeQuery();

                    while (rs.next()) {
                        if (rs.getString("tag").equals(finalTag)) {
                            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That user already has that tag.");
                            rs.close();
                            pstmt.close();
                            return true;
                        }
                    }

                    rs.close();

                    sql = "INSERT INTO tags (user, tag) VALUES (?, ?)";
                    pstmt = c.prepareStatement(sql);
                    pstmt.setString(1, offlinePlayer.getUniqueId().toString());
                    pstmt.setString(2, finalTag);
                    pstmt.executeUpdate();
                    pstmt.close();

                    plugin.cacheUsers();

                    chat.setPlayerPrefix(null, offlinePlayer, finalTag);
                    String setter = "an Admin.";
                    if (sender instanceof Player) setter = ((Player) sender).getDisplayName();
                    if (targetPlayer != null) targetPlayer.sendMessage(ChatColor.GOLD + "Your tag has been set to " + finalTag + ChatColor.GOLD + " by " + setter);
                    sender.sendMessage(ChatColor.GOLD + "You have set a custom tag for " + finalTag + " " + (targetPlayer == null ? offlinePlayer.getName() : targetPlayer.getDisplayName()));
                    return true;
                } catch (Exception e) {
                    plugin.getLogger().severe("/tags set: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                    return true;
                }
            }
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.GREEN + "Console Commands for /tags:");
            sender.sendMessage(ChatColor.GREEN + "/tags set <player> \"[Custom Tag] \"");
            sender.sendMessage(ChatColor.GREEN + "/tags delete <player>");
            return true;
        }
        Player player = (Player) sender;

        String[] groupArr = chat.getPlayerGroups(player);
        LinkedList<String> prefixes = new LinkedList<>();

        List<String> bannedTags = plugin.config.getStringList("bannedTags");

        for (String aGroupArr : groupArr) {
            String curPrefix = chat.getGroupPrefix(player.getWorld(), aGroupArr);
            if (curPrefix == null || curPrefix.length() < 1 || bannedTags.contains(curPrefix)) continue;
            prefixes.add(curPrefix);
        }

        try {
            String sql;
            PreparedStatement pstmt;
            ResultSet rs;
            Connection c = plugin.getConnection();

            sql = "SELECT * FROM tags WHERE user=?";
            pstmt = c.prepareStatement(sql);
            pstmt.setString(1, player.getUniqueId().toString());
            rs = pstmt.executeQuery();

            while (rs.next()) {
                prefixes.add(rs.getString("tag"));
            }

            rs.close();
            pstmt.close();

            if (prefixes.size() < 1) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have access to any tags.");
                return true;
            }

            InventoryManager manager = new InventoryManager("Current Prefix: " + ChatColor.translateAlternateColorCodes('&', chat.getPlayerPrefix(player)), 54, plugin);

            for (int i = 0; i < prefixes.size(); i++) {
                ItemStack item = new ItemStack(Material.NAME_TAG, 1);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', prefixes.get(i)));
                item.setItemMeta(meta);

                manager.withItem(i, item);
            }

            manager.withEventHandler(new InventoryClickHandler() {
                @Override
                public void handle(InventoryClickEvent event) {
                    Player player1 = (Player) event.getWhoClicked();
                    ItemStack clicked = event.getCurrentItem();
                    if (clicked.getType().equals(Material.AIR)) {
                        event.setCancelled(true);
                        return;
                    }

                    ItemMeta meta = clicked.getItemMeta();
                    event.setCancelled(true);
                    String name = meta.getDisplayName();

                    chat.setPlayerPrefix(null, player1, name);
                    player1.closeInventory();
                    player1.sendMessage(ChatColor.GOLD + "Your prefix has been changed to " + ChatColor.WHITE + name);
                }
            });

            manager.show(player);
            return true;
        } catch (Exception e) {
            plugin.getLogger().severe("/tags: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
            return true;
        }
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (args.length == 1) {
            if (sender.hasPermission("customtags.manage")) {
                tabComplete.add("set");
                tabComplete.add("remove");
                tabComplete.add("force");
            }
            if (sender.hasPermission("customtags.reload")) tabComplete.add("reload");
        }
        if (args.length == 2 && (args[0].equals("set") || args[0].equals("remove")) && sender.hasPermission("customtags.manage")) {
            tabComplete.addAll(plugin.cachedTags.values());
        }
        if (args.length == 2 && args[0].equals("force") && sender.hasPermission("customtags.manage")) return null;
        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
