package com.gmail.jd4656.customtags;

import net.milkbowl.vault.chat.Chat;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.util.logging.Level;

public class CustomTags extends JavaPlugin {
    private Chat chat = null;
    private CustomTags plugin;
    private File dbFile;
    private Connection conn = null;
    Map<String, UUID> uuidCache = new HashMap<>();
    Map<String, String> cachedTags = new HashMap<>();
    FileConfiguration config = null;

    @Override
    public void onEnable() {
        plugin = this;
        this.saveDefaultConfig();
        config = this.getConfig();
        config.options().copyDefaults(false);
        this.saveConfig();
        if (!setupChat()) {
            plugin.getLogger().warning("No compatible chat plugin found");
            return;
        }

        plugin.getDataFolder().mkdirs();

        dbFile = new File(plugin.getDataFolder(), "tags.db");
        if (!dbFile.exists()) {
            try {
                dbFile.createNewFile();
            } catch (IOException e) {
                plugin.getLogger().log(Level.SEVERE, "File write error: tags.db");
            }
        }

        try {
            Statement stmt;
            Connection c = plugin.getConnection();

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS tags (user TEXT, tag TEXT)");

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS users (uuid TEXT, name TEXT)");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM users");

            while (rs.next()) {
                uuidCache.put(rs.getString("name").toLowerCase(), UUID.fromString(rs.getString("uuid")));
            }

            stmt.close();
        } catch (Exception e) {
            getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            getLogger().warning("CustomTags failed to load.");
            return;
        }

        plugin.cacheUsers();

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
        this.getCommand("tags").setExecutor(new CommandTags(this));

        getLogger().info("CustomTags loaded.");
    }
    @Override
    public void onDisable() {
        getLogger().info("CustomTags unloaded.");
    }

    void cacheUsers() {
        plugin.cachedTags = new HashMap<>();

        try {
            Connection c = plugin.getConnection();
            Statement stmt = c.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT * FROM users");

            while (rs.next()) cachedTags.put(rs.getString("name").toLowerCase(), rs.getString("name"));

            stmt.close();
        } catch (Exception e) {
            getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            getLogger().warning("Error in cacheUsers");
            return;
        }
    }

    Connection getConnection() throws Exception {
        if (conn == null ) {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + plugin.dbFile);
        }
        return conn;
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        if (rsp != null) chat = rsp.getProvider();
        return chat != null;
    }

    Chat getChat() {
        return chat;
    }
}
