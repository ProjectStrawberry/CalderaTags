package com.gmail.jd4656.customtags;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class EventListeners implements Listener {
    private CustomTags plugin;

    EventListeners(CustomTags p) {
        plugin = p;
    }

    @EventHandler
    public void PlayerJoinEvent(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!plugin.uuidCache.containsKey(player.getName().toLowerCase())) {
            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt;

                if (plugin.uuidCache.containsValue(player.getUniqueId())) {
                    pstmt = c.prepareStatement("UPDATE users SET name=? WHERE uuid=?");
                    pstmt.setString(1, player.getName());
                    pstmt.setString(2, player.getUniqueId().toString());
                    pstmt.executeUpdate();

                    plugin.uuidCache.values().remove(player.getUniqueId());
                    plugin.uuidCache.put(player.getName().toLowerCase(), player.getUniqueId());
                } else {
                    pstmt = c.prepareStatement("INSERT INTO users (uuid, name) VALUES (?, ?)");
                    pstmt.setString(1, player.getUniqueId().toString());
                    pstmt.setString(2, player.getName());
                    pstmt.executeUpdate();

                    plugin.uuidCache.put(player.getName().toLowerCase(), player.getUniqueId());
                }

                pstmt.close();
            } catch (Exception e) {
                plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
            }
        }
    }
}
